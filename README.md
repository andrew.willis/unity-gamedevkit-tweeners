# unity-gamedevkit-tweeners

Wrappers for DG.Tweening that pools the Tweener object instead of destroying it immediately.

## Installation Guide

Asset Store dependencies:

- DOTween (HOTween v2) by Demigiant
  > 1. Import from [Asset Store](https://assetstore.unity.com/packages/tools/animation/dotween-hotween-v2-27676)
  > 2. Setup .asmdef for DOTween using Tools > Demigiant > DOTween Utility Panel > Create ASMDEF. Make sure to include all modules.

### Remarks

UMNP.GamedevKit.Tweeners.asmdef references DOTween.Modules.asmdef using name instead of GUID. Changing the name (although very unlikely) will break the dependency.
