using System;
using DG.Tweening;

namespace UMNP.GamedevKit.Tweeners
{
    [Serializable]
    public struct TweenKey
    {
        public float duration;
        public Ease ease;

        public TweenKey(float duration, Ease ease)
        {
            this.duration = duration;
            this.ease = ease;
        }
    }
}
