using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace UMNP.GamedevKit.Tweeners
{
    /// <summary>
    /// Tweens basic image properties.
    /// </summary>
    [RequireComponent(typeof(Image))]
    public class ImageTweener : BaseTweener
    {
        private Image _image;
        private Color _originalColor = default;

        protected override void Awake()
        {
            base.Awake();
            _image = GetComponent<Image>();
            _originalColor = _image.color;
        }

        /// <summary>
        /// Colorize from the original color.
        /// </summary>
        public void ColorizeFrom(in Color color, in TweenKey tweenKey, in Action onStart = null, in Action onComplete = null)
        {
            if (FromTween == null)
                FromTween = CreateColorTween(_originalColor, color, tweenKey, onComplete);

            PlayFromTween(onStart);
        }

        /// <summary>
        /// Colorize towards the original color.
        /// </summary>
        public void ColorizeTo(in Color color, in TweenKey tweenKey, in Action onStart = null, in Action onComplete = null)
        {
            if (ToTween == null)
                ToTween = CreateColorTween(color, _originalColor, tweenKey, onComplete);

            PlayToTween(onStart);
        }

        private Tweener CreateColorTween(in Color from, in Color to, in TweenKey tweenKey, Action onComplete = null)
        {
            var tweener = _image.DOColor(to, tweenKey.duration)
                .From(from)
                .SetEase(tweenKey.ease)
                .SetUpdate(IgnoreTime)
                .SetAutoKill(false)
                .OnComplete(() => onComplete?.Invoke());

            return tweener;
        }
    }
}