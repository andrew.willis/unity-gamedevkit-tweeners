using System;
using DG.Tweening;
using UnityEngine;

namespace UMNP.GamedevKit.Tweeners
{
    /// <summary>
    /// Tweens basic rotation animation of a rect transform.
    /// </summary>
    [RequireComponent(typeof(RectTransform))]
    public class RotationTweener : BaseTweener
    {
        private RectTransform _rectTransform;
        private Quaternion _anchor = Quaternion.identity;

        protected override void Awake()
        {
            base.Awake();
            _rectTransform = GetComponent<RectTransform>();
            _anchor = _rectTransform.localRotation;
        }

        /// <summary>
        /// Rotate away from the anchor towards given euler angle. Anchor is the original starting rotation.
        /// </summary>
        public void RotateFrom(in Vector3 to, in TweenKey tweenKey, in Action onStart = null, in Action onComplete = null)
        {
            if (FromTween == null)
                FromTween = CreateTween(_anchor.eulerAngles, to, tweenKey, RotateMode.LocalAxisAdd, onComplete);

            PlayFromTween(onStart);
        }

        /// <summary>
        /// Rotate towards the anchor from given euler angle. Anchor is the original starting rotation.
        /// </summary>
        public void RotateTo(in Vector3 from, in TweenKey tweenKey, in Action onStart = null, in Action onComplete = null)
        {
            if (ToTween == null)
                ToTween = CreateTween(from, _anchor.eulerAngles, tweenKey, RotateMode.FastBeyond360, onComplete);

            PlayToTween(onStart);
        }

        private Tweener CreateTween(in Vector3 from, in Vector3 to, in TweenKey tweenKey, RotateMode rotateMode, Action onComplete = null)
        {
            var tweener = _rectTransform.DOLocalRotate(to, tweenKey.duration, rotateMode)
                .From(from)
                .SetEase(tweenKey.ease)
                .SetUpdate(IgnoreTime)
                .SetAutoKill(false)
                .OnComplete(() => onComplete?.Invoke());

            return tweener;
        }
    }
}
