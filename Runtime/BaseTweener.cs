using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace UMNP.GamedevKit.Tweeners
{
    /// <summary>
    /// Base class of basic tweeners, handles boilerplate codes.
    /// </summary>
    public abstract class BaseTweener : MonoBehaviour
    {
        private static List<BaseTweener> _tweeners = new List<BaseTweener>();

        private Tweener _toTween;
        private Tweener _fromTween;
        private bool _ignoreTime = true;

        protected bool IgnoreTime => _ignoreTime;
        protected Tweener ToTween { get => _toTween; set => _toTween = value; }
        protected Tweener FromTween { get => _fromTween; set => _fromTween = value; }

        protected virtual void Awake() => _tweeners.Add(this);

        protected virtual void OnDestroy()
        {
            ResetTweens();
            _tweeners.Remove(this);
        }

        public bool IsPlaying()
        {
            if (ToTween != null)
            {
                if (ToTween.IsPlaying())
                    return true;
            }

            if (FromTween != null)
            {
                if (FromTween.IsPlaying())
                    return true;
            }

            return false;
        }

        public static void ResetAll()
        {
            foreach (var tweener in _tweeners)
                tweener.ResetTweens();
        }

        public void ResetTweens()
        {
            ToTween?.Kill();
            ToTween = null;
            FromTween?.Kill();
            FromTween = null;
        }

        public void SetIgnoreTime(bool ignoreTime) => _ignoreTime = ignoreTime;

        public void ForceComplete()
        {
            ToTween?.Complete();
            FromTween?.Complete();
        }

        protected void PlayToTween(in Action onStart = null)
        {
            if (FromTween != null)
            {
                if (FromTween.IsPlaying())
                    FromTween.Pause();
            }

            onStart?.Invoke();
            ToTween.Restart();
        }

        protected void PlayFromTween(in Action onStart = null)
        {
            if (ToTween != null)
            {
                if (ToTween.IsPlaying())
                    ToTween.Pause();
            }

            onStart?.Invoke();
            FromTween.Restart();
        }
    }
}
