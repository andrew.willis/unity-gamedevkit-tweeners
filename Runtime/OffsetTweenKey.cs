using System;
using UnityEngine;

namespace UMNP.GamedevKit.Tweeners
{
    [Serializable]
    public struct OffsetTweenKey
    {
        public TweenKey tweenKey;
        public Vector2 offset;

        public OffsetTweenKey(TweenKey tweenKey, Vector2 offset)
        {
            this.tweenKey = tweenKey;
            this.offset = offset;
        }
    }
}