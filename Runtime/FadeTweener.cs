using System;
using DG.Tweening;
using UnityEngine;

namespace UMNP.GamedevKit.Tweeners
{
    /// <summary>
    /// Tweens basic fade in and out on CanvasGroup
    /// </summary>
    [RequireComponent(typeof(CanvasGroup))]
    public class FadeTweener : BaseTweener
    {
        private CanvasGroup _canvasGroup;

        protected override void Awake()
        {
            base.Awake();
            _canvasGroup = GetComponent<CanvasGroup>();
        }

        public void FadeOut(in TweenKey tweenKey, in Action onStart = null, Action onComplete = null)
        {
            if (FromTween == null)
            {
                FromTween = CreateTween(
                    1.0f,
                    0.0f,
                    tweenKey,
                    () =>
                    {
                        _canvasGroup.blocksRaycasts = false;
                        onComplete?.Invoke();
                    }
                );
            }

            PlayFromTween(onStart);
        }

        public void FadeIn(in TweenKey tweenKey, Action onStart = null, Action onComplete = null)
        {
            if (ToTween == null)
                ToTween = CreateTween(0.0f, 1.0f, tweenKey, onComplete);

            PlayToTween(
                () =>
                {
                    _canvasGroup.blocksRaycasts = true;
                    onStart?.Invoke();
                }
            );
        }

        private Tweener CreateTween(float from, float to, in TweenKey tweenKey, Action onComplete = null)
        {
            var tweener = _canvasGroup.DOFade(to, tweenKey.duration)
                .From(from)
                .SetEase(tweenKey.ease)
                .SetUpdate(IgnoreTime)
                .SetAutoKill(false)
                .OnComplete(() => onComplete?.Invoke());

            return tweener;
        }
    }
}
